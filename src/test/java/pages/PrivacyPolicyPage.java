package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class PrivacyPolicyPage {

    private static final SelenideElement PRIVACY_POLICY_PAGE_TITLE = $("div[class*=title]>h1");

    public String getPrivacyPolicyPageTitle(){
        return PRIVACY_POLICY_PAGE_TITLE.text();
    }
}
