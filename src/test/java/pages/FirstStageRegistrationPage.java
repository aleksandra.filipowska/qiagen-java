package pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FirstStageRegistrationPage extends DefaultPage {

    private static final SelenideElement FIRST_STAGE_REGISTRATION_PAGE_TITLE = $(By.xpath("//h2[contains(text(),'Create My QIAGEN account')]"));
    private static final SelenideElement EMAIL_ADDRESS = $(By.name("email"));
    private static final SelenideElement PASSWORD = $("[type='password']");
    private static final SelenideElement LOCATION = $(By.name("country"));
    private static final SelenideElement TELEPHONE = $("[formcontrolname='telephoneNumber']");
    private static final SelenideElement TITLE = $(By.xpath("//label[contains(text(),'Title')]/select"));
    private static final SelenideElement ACADEMIC_TITLE = $(By.xpath("//label[contains(text(),'Academic title')]/select"));
    private static final SelenideElement FIRST_NAME = $("[formcontrolname='firstName']");
    private static final SelenideElement LAST_NAME = $("[formcontrolname='lastName']");

    public String getFistStageRegistrationPageTitle() {
        return FIRST_STAGE_REGISTRATION_PAGE_TITLE.text();
    }

    public void fillTheRegistrationForm() {

    }

    public void entereEmail(String email) {
        $(EMAIL_ADDRESS).sendKeys(email);
    }

    public void enterPassword(String password) {
        $(PASSWORD).sendKeys(password);
    }

    public void setLocation(String locationCode) {
        $(LOCATION).click();
        $(LOCATION).$(String.format("%s_%s_%s_s", "option[value='", locationCode, "']"));

    }

    public void enterTelephoneNumber() {

    }

    public void setTitle() {

    }

    public void setAcademicTitle() {

    }

    public void enterFirstName() {

    }

    public void enterLastName() {

    }

    public void openFacebookFeedbackLite() {

    }


}
