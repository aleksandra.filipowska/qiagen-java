package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class MyQiagenPage extends DefaultPage {


    private static final SelenideElement REGISTRATION_BUTTON = $("section.register > div > a");


    public FirstStageRegistrationPage goToFirstStageRegistrationPage() {
        REGISTRATION_BUTTON.click();
        return new FirstStageRegistrationPage();
    }
}

