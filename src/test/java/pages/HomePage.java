package pages;


import com.codeborne.selenide.*;

import static com.codeborne.selenide.Selenide.*;


public class HomePage extends DefaultPage {


    private static final SelenideElement COMPANY_LOGO = $("[class='logo']>img");

    public boolean isCompanyLogoDisplayed() {
        return $(COMPANY_LOGO).isDisplayed();
    }
}
