package pages;

import com.codeborne.selenide.SelenideElement;
import config.DefaultSeleniumWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public abstract class DefaultPage extends DefaultSeleniumWebDriver {

    private static final SelenideElement MY_QIAGEN_PAGE = $("li[class*='not-loggged-in']>a");
    private static final SelenideElement FAQ_PAGE = $(By.xpath("//a[contains(text(),'Questions & Answers')]"));
    private static final SelenideElement HOW_TO_REGISTER_PAGE = $(By.xpath("//a[contains(text(),'How to register')]"));
    private static final SelenideElement PRIVACY_POLICY_PAGE = $("a[href*='privacy-policy']");
    private static final SelenideElement SECOND_STAGE_REGISTRATION_PAGE = $(By.xpath("//button[contains(text(),'Next')]"));

    protected WebDriverWait driverWait;

    public DefaultPage() {
        this.driverWait = new WebDriverWait(getWebDriver(), 20);
    }

    public HomePage goToHomePage() {
        return new HomePage();
    }

    public MyQiagenPage goToMyQiagenPage() {
        MY_QIAGEN_PAGE.click();
        return new MyQiagenPage();
    }

    public FaqPage goToFaqPage() {
        FAQ_PAGE.click();
        return new FaqPage();
    }

    public HowToRegisterPage goToHowToRegisterPage() {
        HOW_TO_REGISTER_PAGE.click();
        return new HowToRegisterPage();
    }

    public PrivacyPolicyPage goToPrivacyPolicyPage() {
        PRIVACY_POLICY_PAGE.click();
        return new PrivacyPolicyPage();
    }

    public SecondStageRegistrationPage goToSecondStageRegistrationPage() {
        SECOND_STAGE_REGISTRATION_PAGE.click();
        return new SecondStageRegistrationPage();
    }


}
