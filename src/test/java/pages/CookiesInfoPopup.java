package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CookiesInfoPopup extends DefaultPage {
    private static final SelenideElement COOKIES_CONTAINER = $("[class*='privacy-warning']");
    private static final SelenideElement ACCEPT_COOKIES_BUTTON = $("[class*='privacy-warning']>div>a[onclick*='cookiesAccepted']");
    
    public boolean isCookiesContainerDisplayed(){return COOKIES_CONTAINER.isDisplayed();}
    
    public void acceptCookies(){
        if(isCookiesContainerDisplayed()){
            ACCEPT_COOKIES_BUTTON.click();
            COOKIES_CONTAINER.should(Condition.disappear);
        }
    }
}
