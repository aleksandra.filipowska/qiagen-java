package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class FaqPage extends DefaultPage {

    private static final SelenideElement FAQ_PAGE_TITLE =$("div[class*='introduction'] >div>h1");

    public String faqPageTitle() {
        return FAQ_PAGE_TITLE.text();
    }
}
