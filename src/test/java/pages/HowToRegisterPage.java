package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HowToRegisterPage extends DefaultPage {

    private static final SelenideElement REGISTRATION_SECRET_LINK = $("[class*='secret-link-title']");

    public String RegistrationSecretLinkText() {
        return REGISTRATION_SECRET_LINK.text();
    }
}
