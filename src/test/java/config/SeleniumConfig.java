package config;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class SeleniumConfig {

    public static Properties initConfig(String fileName) {
        Properties defaultProperties = new Properties();
        defaultProperties.setProperty("LOCAL_REMOTE", "remote");
        defaultProperties.setProperty("WEB_DRIVER_REMOTE_BROWSER_NAME", "chrome-linux");
        defaultProperties.setProperty("WEB_DRIVER_REMOTE_ADDRESS_QIAGEN", "http://selenium-hub:4444/wd/hub");
        defaultProperties.setProperty("BASE_URL", "https://qa-preproduction.qiagen.com/pl/");
        defaultProperties.setProperty("SYSTEM_VERSION", System.getProperty("os.name"));

        Properties config = new Properties(defaultProperties);

        try {
            config.load(new FileInputStream(fileName));
        } catch (IOException e) {
            log.info(String.format("%s not found, see seleniumConfig.cfg.example for reference. Falling back to default config",
                    fileName));
        }

        return config;
    }

    private SeleniumConfig() {
    }
}
