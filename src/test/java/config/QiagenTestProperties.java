package config;

import java.util.Properties;

public class QiagenTestProperties {

    private static final Properties config = SeleniumConfig.initConfig("seleniumConfig.cfg");
    public static final String LOCAL_REMOTE = config.getProperty("LOCAL_REMOTE") ;
    public static final String WEB_DRIVER_REMOTE_BROWSER_NAME = config.getProperty("WEB_DRIVER_REMOTE_BROWSER_NAME");
    public static final String WEB_DRIVER_REMOTE_ADDRESS_QIAGEN=  config.getProperty("WEB_DRIVER_REMOTE_ADDRESS_QIAGEN");
    public static final String BASE_URL =  config.getProperty("BASE_URL");
    public static final String SYSTEM_VERSION =  config.getProperty("SYSTEM_VERSION");
}
