package config;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.log4testng.Logger;
import pages.CookiesInfoPopup;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.screenshot;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static config.QiagenTestProperties.*;

@Slf4j
public class DefaultSeleniumWebDriver {

    private Logger logger = Logger.getLogger(this.getClass());

    @BeforeClass
    public void setUp() {
        WebDriver driver;
        if (LOCAL_REMOTE.equals("local")) {
            if (SYSTEM_VERSION.toLowerCase().contains("linux")) {
                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver-linux-64bit");
                driver = new ChromeDriver();
            } else if (SYSTEM_VERSION.toLowerCase().contains("windows")) {
                System.setProperty("webdriver.gecko.driver", "divers/geckodriver-windows-32bit.exe");
                driver = new FirefoxDriver();
            } else {
                logger.info("Incorrect OS");
                return;
            }

        } else {
            DesiredCapabilities capabilities;
            switch (WEB_DRIVER_REMOTE_BROWSER_NAME) {
                case "firefox-linux":
                    capabilities = DesiredCapabilities.firefox();
                    capabilities.setPlatform(Platform.LINUX);
                    break;
                case "chrome-linux":
                    capabilities = DesiredCapabilities.chrome();
                    capabilities.setPlatform(Platform.LINUX);
                    break;
                default:
                    capabilities = DesiredCapabilities.firefox();
                    capabilities.setPlatform(Platform.ANY);
                    break;
            }
            try {
                driver = new RemoteWebDriver(new URL(WEB_DRIVER_REMOTE_ADDRESS_QIAGEN), capabilities);
            } catch (MalformedURLException ex) {
                logger.info((ex));
                return;
            }
        }
        WebDriverRunner.setWebDriver(driver);
        setSelenideConfig();
        driver.manage().timeouts().setScriptTimeout(100000, TimeUnit.SECONDS);
    }

    @AfterClass
    public void tearDown() {
        getWebDriver().close();
    }


    @BeforeMethod
    public void goToDefaultPage() {
        getWebDriver().manage().deleteAllCookies();
        open(BASE_URL);
        new CookiesInfoPopup().acceptCookies();
    }

    private static void setSelenideConfig() {
        Configuration.timeout = 2000000;
        Configuration.reportsFolder = "target/selenide";
    }


    @AfterMethod
    public void tearDown(Method method) {
        takeScreenshot(method);
    }

    private void takeScreenshot(Method method) {
        String timeStamp;
        String screenShotName;
        timeStamp = new SimpleDateFormat("yyyymmdd HHmmss").format(Calendar.getInstance().getTime());
        screenShotName = String.format("%s_$s", method.getName(), timeStamp);
        screenshot(screenShotName);
    }


}
