package tests;

import config.DefaultSeleniumWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.FirstStageRegistrationPage;
import pages.HomePage;
import pages.MyQiagenPage;
import pages.PrivacyPolicyPage;

public class RegistrationTest extends DefaultSeleniumWebDriver {

    private static final String EXPECTED_PRIVACY_POLICY_PAGE_TITLE = "Privacy Policy";
    private MyQiagenPage myQiagenPage;
    private FirstStageRegistrationPage firstStageRegistrationPage;
    private HomePage homePage;
    private PrivacyPolicyPage privacyPolicyPage;

    @BeforeMethod
    private void setUpTest() {
        homePage = new HomePage();
        myQiagenPage = homePage.goToMyQiagenPage();
        firstStageRegistrationPage = myQiagenPage.goToFirstStageRegistrationPage();
    }


    //    @Test
//    public void shouldSuccessfullyRegisterNewUser() {
//
//    }
//
//    @Test
//    public void shouldFailToRegisterNewUser() { // zly email zle haslo zle rozne
//
//    }
//
//    @Test
//    public void shouldFailToRegisterUserAlreadyRegistered() {
//
//    }
    @Test
    public void shouldRedirectToPrivacyPolicyPage() {
        privacyPolicyPage = homePage.goToPrivacyPolicyPage();
        Assert.assertEquals(privacyPolicyPage.getPrivacyPolicyPageTitle(), EXPECTED_PRIVACY_POLICY_PAGE_TITLE);
    }

}
