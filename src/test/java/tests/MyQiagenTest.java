package tests;

import config.DefaultSeleniumWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;

import static com.codeborne.selenide.Selenide.switchTo;

public class MyQiagenTest extends DefaultSeleniumWebDriver {
    private static final String EXPECTED_REGISTRATION_SECRET_MESSAGE = "My QIAGEN Registration";
    private static final String EXPECTED_FAQ_PAGE_TITLE = "Shop and Login FAQ's";
    private static final String EXPECTED_FIRST_STAGE_REGISTRATION_PAGE_TITLE = "Create My QIAGEN account - in just 2 steps!";

    private HomePage homePage;
    private MyQiagenPage myQiagenPage;
    private FaqPage faqPage;
    private HowToRegisterPage howToRegisterPage;
    private FirstStageRegistrationPage firstStageRegistrationPage;

    @BeforeMethod
    private void setUpTest() {
        homePage = new HomePage();
        myQiagenPage = homePage.goToMyQiagenPage();
    }

    @Test
    public void shouldRedirectToFaqPage() {
       faqPage =  myQiagenPage.goToFaqPage();
       Assert.assertEquals(faqPage.faqPageTitle(), EXPECTED_FAQ_PAGE_TITLE);
    }

    @Test
    public void shouldRedirectToHowToRegisterPage() {
        howToRegisterPage = myQiagenPage.goToHowToRegisterPage();
        switchTo().window(1);
        Assert.assertEquals(howToRegisterPage.RegistrationSecretLinkText(),EXPECTED_REGISTRATION_SECRET_MESSAGE);
    }

    @Test
    public void shouldGoToFirstStageRegistrationPage() {
        firstStageRegistrationPage = myQiagenPage.goToFirstStageRegistrationPage();
        Assert.assertEquals(firstStageRegistrationPage.getFistStageRegistrationPageTitle(), EXPECTED_FIRST_STAGE_REGISTRATION_PAGE_TITLE);
    }


}
