FROM maven:3-jdk-11
WORKDIR /qiagen-java
ADD /src src
ADD pom.xml .
CMD ["mvn","verify"]
